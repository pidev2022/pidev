package esprit.tn.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import esprit.tn.dao.DomaineActiviteRepository;
import esprit.tn.entities.DomaineActivite;
import esprit.tn.repository.DomaineActiviteMetierImpl;
import esprit.tn.repository.IDomaineActiviteMetier;

@RestController

@RequestMapping("/api/v1/domaineActivite")
public class DomaineActiviteController {

	@Autowired
	private DomaineActiviteMetierImpl domaineActiviteMetierImpl;



	@RequestMapping("/domaineActivite")
	public List<DomaineActivite> getDomaineActivite() {
		List<DomaineActivite> domaineActivities = domaineActiviteMetierImpl.listDomaineActivites();
		return domaineActivities;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public String deleteDomaineActivite(@RequestParam(name = "id") Long id) {
		domaineActiviteMetierImpl.deleteDomaineActivite(id);
		return "id " + id+ " est supprimé";
	}
	@RequestMapping(value = { "/add" }, method = RequestMethod.POST)
	public String addDomaineActivite(@RequestBody DomaineActivite domaineActivite) {
		domaineActiviteMetierImpl.saveDomaineActivite(domaineActivite);
		return "DomaineActivite ajouté avec succes" ;
	}
	@RequestMapping(value = { "/update" }, method = RequestMethod.PUT)
	public String updateDomaineActivite(@Valid @RequestBody DomaineActivite domaineActivite) {
		domaineActiviteMetierImpl.updateDomaineActivite(domaineActivite);
		return "DomaineActivite modifié avec succes" ;
	}
	@RequestMapping(value = "/findById")
	public DomaineActivite getDomaineActiviteById(@RequestParam(name = "id") Long id) {
		return domaineActiviteMetierImpl.getDomaineActiviteById(id);
		
	}



}
