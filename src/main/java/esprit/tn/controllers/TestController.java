package esprit.tn.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@RequestMapping("/root")
	public String getRoot() {

		return "hello root";
	}

	@RequestMapping("/employee")
	public String getEmloyee() {

		return "hello employee";
	}

	@RequestMapping("/entreprise")
	public String geteEtreprise() {

		return "hello entreprise";
	}
	
	@RequestMapping("/public")
	public String getPublic() {

		return "hello everyone";
	}

}