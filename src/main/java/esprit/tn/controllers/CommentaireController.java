package esprit.tn.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import esprit.tn.dao.CommentaireRepository;
import esprit.tn.entities.Commentaire;


@RestController
@RequestMapping("/api/v1/commentaire")
@CrossOrigin(origins="http://localhost:4200")
public class CommentaireController {
	@Autowired
	private CommentaireRepository comRepo;
	
	
	@GetMapping
	public List<Commentaire> list(){
		return comRepo.findAll();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public void create(@RequestBody Commentaire com) {
		comRepo.save(com);
	}
	
	@GetMapping("/{id}")
	public Commentaire get(@PathVariable("id") Long id) {
		return comRepo.findById(id).get();
	}

}
