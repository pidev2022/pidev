package esprit.tn.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import esprit.tn.dao.EntrepriseRepository;
import esprit.tn.entities.DomaineActivite;
import esprit.tn.entities.Entreprise;
import esprit.tn.entities.User;
import esprit.tn.repository.DomaineActiviteMetierImpl;
import esprit.tn.repository.EntrepriseMetierImpl;
import esprit.tn.repository.IEntrepriseMetier;

@RestController

@RequestMapping("/api/v1/entreprise")
public class EntrepriseController {

	@Autowired
	private IEntrepriseMetier entRepo;



	@PostMapping("/ajouterEntreprise")
	@ResponseBody
	public ResponseEntity ajouterEntreprise(@RequestBody Entreprise entreprise) {
		if (!entRepo.isValidEmailAddress(entreprise.getEmail())) {
            return new ResponseEntity("Email is invalid", HttpStatus.FORBIDDEN);
		}
        return new ResponseEntity<Entreprise>(entRepo.addEntreprise(entreprise), HttpStatus.OK);

	}

	@GetMapping("/getEntrepriseById/{identreprise}")
	@ResponseBody
	public Entreprise getEntrepriseById(@PathVariable("identreprise") Long entrepriseId) {

		return entRepo.retrieveEntrepriseById(entrepriseId);
	}
	@DeleteMapping("/deleteEntrepriseById/{identreprise}")

	@ResponseBody
	public void deleteEntrepriseById(@PathVariable("identreprise") Long entrepriseId) {
		entRepo.deleteEntrepriseById(entrepriseId);
	}
	
	@GetMapping("/getAllEntreprise")
	@ResponseBody
	public List<Entreprise> getAllEntreprise() {
		List<Entreprise> Entreprises = entRepo.retrieveAllEntreprise();;
		return Entreprises;


	}
	
	@RequestMapping(value = { "/entreprise/update" }, method = RequestMethod.PUT)
	public String updateEntreprise(@Valid @RequestBody Entreprise entreprise) {
		entRepo.updateEntreprise(entreprise);
		return "Entreprise modifié avec succes" ;
	}

	@PutMapping(value = "/affecterDomaineActivite/{idda}/{identreprise}")
	public void affecterDomaineActivite(@PathVariable("idda") Long daId,
			@PathVariable("identreprise") Long entrepriseId) {
		entRepo.affecterDomaineActivite(daId, entrepriseId);
	}

	@GetMapping(value = "/getAllDomaineActiviteNamesByEntreprise/{identreprise}")
    @ResponseBody
	public List<String> getAllDomaineActiviteNamesByEntreprise(@PathVariable("identreprise") Long entrepriseId)  {
		return entRepo.getAllDomaineActiviteNamesByEntreprise(entrepriseId);
	}
}
