package esprit.tn.controllers;


import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import esprit.tn.dao.VoyageRepository;
import esprit.tn.entities.Voyage;

@RestController
public class VoyageController {
	@Autowired
	private VoyageRepository voyageRep;
	@Autowired
	private HttpSession session;
	@RequestMapping("/voyages")
	public String index(Model model, @RequestParam(name = "p", defaultValue = "0") int p,
			@RequestParam(name = "s", defaultValue = "8") int s,
			@RequestParam(name = "mc", defaultValue = "") String mc) {
		Page<Voyage> voyages = voyageRep.findAll(PageRequest.of(p, s));
		model.addAttribute("voyages", voyages.getContent());

		model.addAttribute("pages", new int[voyages.getTotalPages()]);
		model.addAttribute("size", s);
		model.addAttribute("pageCourant", p);
		model.addAttribute("mc", mc);
		return "Operation done successfully";
	}
	private boolean savevoyage(Voyage voyage, BindingResult result, Model model) {		

		if (result.hasErrors()) {
			model.addAttribute("voyage test", voyage);
			return false;
		}
		Voyage tmpVoyage = new Voyage() ;
		tmpVoyage.setLabel(voyage.getLabel());
		tmpVoyage.setDescription(voyage.getDescription());
		tmpVoyage.setDateDeb(voyage.getDateDeb());
		tmpVoyage.setDateFin(voyage.getDateFin());
		tmpVoyage.setType(voyage.getType());
		tmpVoyage.setDateDeb(voyage.getDateDeb());
		tmpVoyage.setDateFin(voyage.getDateFin());
		tmpVoyage.setIdVoyage(voyage.getIdVoyage());

		voyageRep.save(tmpVoyage);
		
		

		return true;
	}

	@RequestMapping(value = { "/voyages/add" }, method = RequestMethod.POST)
	public String addVoyage(@Valid @RequestBody Voyage voyage, BindingResult result, Model model) {
		if (voyageRep.findByIdVoyage(voyage.getIdVoyage()) != null)
			result.rejectValue("id_voyage", "error.id_voyage", "ID de voyage exist déjà !.");

		if (savevoyage(voyage, result, model))
			model.addAttribute("add done", "voyage ajouté !");
		else
			model.addAttribute("add Failed", true);

		return index(model, 0, 8, "");
	}
	
	@RequestMapping(value = "/voyages/update", method = RequestMethod.PUT)
	public String updateVoyage(@Valid Voyage voyage, BindingResult result, Model model,@RequestParam(name = "id_voyage", defaultValue = "0") Long id_voyage) {
		if ((voyageRep.findVoyageById(id_voyage)) == null) {
			result.rejectValue("id_voyage", "error.voyage", "Ce voyage n'exite pas !");
		} else if (savevoyage(voyage, result, model))
			model.addAttribute("update done", "voyage " + voyage.getIdVoyage() + " est Mis à jour!");
		else
			model.addAttribute("updateFailed", true);

		return index(model, 0, 8, "");
	}
	
	@RequestMapping(value = "/deleteVoyage",method = RequestMethod.DELETE)
	public void deleteVoyage(Model model, @RequestParam(name = "id_voyage", defaultValue = "0") Long id_voyage) {
		voyageRep.delete(voyageRep.findVoyageById(id_voyage));
		//model.addAttribute("delete done", "voyage " + id_voyage + " est supprimé");
		//return index(model, 0, 8, "");
	}
	
	@RequestMapping(value="/voyages", method = RequestMethod.GET)
	public List<Voyage> getVoyage(Model model) {
		List<Voyage> voyages = voyageRep.findAll();
		return voyages;
	}
	
	@RequestMapping(value = "/voyage/get", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Voyage getVoyage(@RequestParam(name = "label") String label) {
		return voyageRep.findByVoyageLabel(label);
	}
}
