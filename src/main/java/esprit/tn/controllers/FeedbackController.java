package esprit.tn.controllers;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import esprit.tn.entities.Feedback;
import esprit.tn.repository.FeedbackMetierImpl;
@RestController
public class FeedbackController {

	@Autowired
	FeedbackMetierImpl feedbackmetImpl;
	
	@GetMapping("/getFeedbacks")
	@ResponseBody
	public List<Feedback> getAllFeedbacks() {
		List<Feedback> feedbacks = feedbackmetImpl.listFeedbacks();
		return feedbacks;
	}
	@RequestMapping(value = { "/feedback/add/{user}" }, method = RequestMethod.POST)
	public String addFeedback(@Valid @RequestBody Feedback fdbk, @PathVariable String user) throws IOException, InterruptedException {
		fdbk.setSentiment(SentimentalAnalysisController.getSentiment(fdbk.getComments()));
		feedbackmetImpl.addFeedback(fdbk,user);
		return "feedback ajouté avec succes" ;
	}
}
