package esprit.tn.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import esprit.tn.dao.RoleRepository;
import esprit.tn.dao.UserRepository;
import esprit.tn.dao.UserRolesRepository;
import esprit.tn.entities.Role;
import esprit.tn.entities.User;
import esprit.tn.entities.UsersRoles;

@RestController
public class UserController {
	@Autowired
	private UserRepository usrRep;
	@Autowired
	private RoleRepository rolRep;
	@Autowired
	private UserRolesRepository usrRolRep;
	

	@RequestMapping("/users")
	public List<User> getAllUsers() {

		return usrRep.findAll();
	}

	@RequestMapping(value = { "/users/add" }, method = RequestMethod.POST)
	public String adduser(@Valid @RequestBody User user) {
		if (usrRep.findByUserName(user.getUsername()) != null)
			return "Nom d'utilisateur déja exist !.";
		Collection<UsersRoles> userRoles = user.getRoles();
		if (userRoles == null || userRoles.size() == 0)
			return "Vous devez affecter au moins une permission !.";
		if (saveuser(user))
			return "user ajouté !";
		return "échec d'ajout user!";

	}

	@RequestMapping(value = "/users/update", method = RequestMethod.POST)
	public String updateuser(@Valid @RequestBody User user, @RequestParam(name = "password") String pwd) {
		if ((usrRep.findByUserName(user.getUsername())) == null) {
			return "Cet utilisateur n'exist pas !";
		}
		User tmpUsr = usrRep.findByUserName(user.getUsername());

		if (!new BCryptPasswordEncoder().matches(pwd, tmpUsr.getPassword())) {
			return "invalid pwd !";
		}
		tmpUsr.setActive(user.getActive());
		tmpUsr.setEmail(user.getEmail());
		tmpUsr.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

		if (usrRep.save(tmpUsr) != null)
			return "user " + user.getUsername() + " est Mis à jour!";

		return "échec d'update user !";

	}

	private Boolean saveuser(User user) {

		User tmpUser = new User();
		tmpUser.setActive(user.getActive());
		tmpUser.setUsername(user.getUsername());
		tmpUser.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		tmpUser.setEmail(user.getEmail());
		tmpUser = usrRep.save(tmpUser);
		List<UsersRoles> tmpRoleList = new ArrayList<>();

		for (UsersRoles x : user.getRoles()) {

			Role tmpRole = new Role();

			if ((rolRep.findByRole(x.getRole().getRole())) == null) {
				tmpRole = saverole(x.getRole());

			} else {
				tmpRole = rolRep.findByRole(x.getRole().getRole());
			}
			UsersRoles tmpUserRole = new UsersRoles();
			tmpUserRole.setRole(tmpRole);
			tmpUserRole.setUser(tmpUser);

			tmpRoleList.add(tmpUserRole);
		}
		usrRolRep.saveAll(tmpRoleList);
		return true;
	}

	private Role saverole(Role role) {

		Role tmpRole = new Role();

		tmpRole.setRole(role.getRole());
		tmpRole.setDesignation(role.getDesignation());
		return rolRep.save(tmpRole);

	}

	@RequestMapping(value = "/users/delete")
	public String deleteuser(Model model, @RequestParam(name = "username", defaultValue = "0") String username) {
		User user = usrRep.getOne(username);
		usrRolRep.deleteAll(user.getRoles());
		usrRep.delete(user);
		return "user " + username + " est supprimé";
	}

	@RequestMapping(value = "/users/get", method = RequestMethod.POST)
	public @ResponseBody User getuser(@RequestParam(name = "username") String username) {
		return usrRep.findByUserName(username);
	}

	@RequestMapping(value = "/users/storeuserrole", method = RequestMethod.POST)
	public @ResponseBody String[] storeuserrole(@RequestParam(name = "role") String r,
			@RequestParam(name = "user") String u) {
		Role role = rolRep.findByRole(r);

		if (role == null) {
			return new String[] { "Role n'existe pas !" };

		}
		User user = usrRep.findByUserName(u);
		if (user == null) {
			return new String[] { "User n'existe pas !" };

		}
		UsersRoles tmpUserRole = new UsersRoles();
		tmpUserRole.setRole(role);
		tmpUserRole.setUser(user);

		usrRolRep.save(tmpUserRole);

		return new String[] { "le user : " + u + " a le role " + r + " maintenant ! " };
	}

	@RequestMapping(value = "/users/blocker", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String[] removeuserrole(@RequestParam(name = "user", defaultValue = "") String u) {
		User user = usrRep.findByUserName(u);
		if (user == null) {
			return new String[] { "User n'existe pas !" };
		}
		usrRolRep.deleteAll(user.getRoles());
		return new String[] { "le user : " + u + " est bloqué maintenant ! " };

	}

	


	@RequestMapping(value = "/roles/addrole", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String[] addrole(@RequestParam(name = "role") String r) {
		if (r.length() <= 3)
			return new String[] { "Le role doit etre au moins 4c." };
		if (rolRep.findByRole(r) != null)
			return new String[] { "Le role déja exist !" };
		Role ro = rolRep.save(new Role(r));
		return new String[] { "", ro.getId() + "" };
	}

	

	@RequestMapping(value = "/roles/deleterole", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String[] addrole(@RequestParam(name = "id") Long id) {
		
		rolRep.delete(new Role(id,""));
		return new String[] {};
	}

	@RequestMapping("/roles")
	public List<Role> getRoles(Model model) {
		List<Role> roles = rolRep.findAll();
		return roles;
	}
	


	public Optional findUserByResetToken(String resetToken) {
		return usrRep.findByResetToken(resetToken);
	}

}