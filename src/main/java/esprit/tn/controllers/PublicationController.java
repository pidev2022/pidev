package esprit.tn.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import esprit.tn.dao.CommentaireRepository;
import esprit.tn.dao.PublicationRepository;
import esprit.tn.entities.Commentaire;
import esprit.tn.entities.Publication;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;







@RestController
@RequestMapping("/api/v1/publication")
@CrossOrigin(origins="http://localhost:4200")
public class PublicationController {
	
	@Autowired
	private PublicationRepository pubRepo;
	@Autowired
	private CommentaireRepository comRepo;
	
	@GetMapping
	public List<Publication> list(){
		return pubRepo.findAll();
	}
	
	@GetMapping("/best")
	public JSONObject best(){
		
		JSONObject body = new JSONObject();

		Map<Long, Integer> map = new HashMap<>();
		int d=0;
		List<Publication> l =  pubRepo.findAll();
		for (Publication list : l) {
			int x = list.getCommentaire().size();
			if(x>d) {
            d=list.getCommentaire().size();
            body.put("name", list.getId());
            body.put("value", x);
            }
            
        }
		return body;
		
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public void create(@RequestBody Publication pub) {
		List<Commentaire> listcommentaire = new ArrayList<>();
	    Publication p = pubRepo.save(pub);
	    if(p.getCommentaire() != null) {
	    	
	    
		pub.getCommentaire().forEach(x->{
			Commentaire c = new Commentaire();
			c.setDetails(x.getDetails());
			c.setPublication(p);
			listcommentaire.add(c);
		});
		
		
		comRepo.saveAll(listcommentaire);
	    }
		//pubRepo.save(pub);
		
		
	}
	
	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		 pubRepo.deleteById(id);
	}
	/**
	@PutMapping(value = "/{id}")
	public void update(@PathVariable("id") Long id, @RequestBody Publication pub) {
		pub = pubRepo.findById(id).get();
		if(pub != null) {
			pubRepo.save(null);
		}
		
	}
	*/
	
	@PutMapping("/{id}")
	  public ResponseEntity<Publication> update(@PathVariable("id") long id, @RequestBody Publication pub) {
	    Optional<Publication> pubData = pubRepo.findById(id);
	    if (pubData.isPresent()) {
	      Publication _pub = pubData.get();
	      _pub.setTitre(pub.getTitre());
	      _pub.setDetails(pub.getDetails());
	      _pub.setDate(pub.getDate());
	      _pub.setAuthor(pub.getAuthor());
	      _pub.setCommentaire(pub.getCommentaire());
	      return new ResponseEntity<>(pubRepo.save(_pub), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	
	
	
	@PutMapping("/likes/{id}")
	  public ResponseEntity<Publication> like(@PathVariable("id") long id, @RequestBody Publication pub) {
	    Optional<Publication> pubData = pubRepo.findById(id);
	    if (pubData.isPresent()) {
	      Publication _pub = pubData.get();
	      _pub.setLikes(pub.getLikes());
	      return new ResponseEntity<>(pubRepo.save(_pub), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	
	
	
	
	@PutMapping("/commentaire/{id}")
	  public ResponseEntity<Publication> addcommentaire(@PathVariable("id") long id, @RequestBody Publication pub) {
	    Optional<Publication> pubData = pubRepo.findById(id);
	    if (pubData.isPresent()) {
	      Publication _pub = pubData.get();
	      _pub.setCommentaire(pub.getCommentaire());
	      return new ResponseEntity<>(pubRepo.save(_pub), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	 
	
	
	@GetMapping("/{id}")
	public Publication get(@PathVariable("id") Long id) {
		return pubRepo.findById(id).get();
	}
	/**
	@PostMapping("/badword")
	public String bad(@RequestBody String body) {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		
	    MediaType mediaType = MediaType.parse("text/plain");
	    Request request = new Request.Builder()
	      .url("https://api.apilayer.com/bad_words?censor_character=censor_character")
	      .addHeader("apikey", "acZPFZQXQQ8G4uIoXbhZfnDusjt9gFAZ")
	      .method("POST", body})
	    Response response2 = client.newCall(request).execute();
	    return response.body().string();
	}*/


@PostMapping("/badWordss")
public String badWords(@RequestBody String text)  throws IOException, InterruptedException {
	JSONObject body = new JSONObject();
	body.put("text", text);
	HttpRequest request = HttpRequest.newBuilder()
			.uri(URI.create("https://api.apilayer.com/bad_words?censor_character=*"))
			.header("Content-Type", "application/json")
			.header("apikey", "v6DtE8K9MwD38MMuHKPvsccx0wyZmdtc")
			.method("POST", HttpRequest.BodyPublishers.ofString(body.toString()))
			.build();
	HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
	//JSONObject obj = new JSONObject(response.body());
	//JSONObject result = new JSONObject(obj);

	return response.body().toString();

	
}



}
