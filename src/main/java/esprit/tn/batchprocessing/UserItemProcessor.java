package esprit.tn.batchprocessing;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import esprit.tn.entities.User;

public class UserItemProcessor implements ItemProcessor<User, User> {

	private static final Logger log = LoggerFactory.getLogger(UserItemProcessor.class);
	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private Environment env;

	@Autowired
	private VelocityEngine engine;

	private String attachment;

	public UserItemProcessor(String attachment) {
		this.attachment = attachment;
	}

	public User process(User user) throws Exception {
		Map<String, Object> model = new HashMap<>();

		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		String email = user.getEmail();
		try {
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
			mimeMessageHelper.setSubject("Your Code");
			model.put("name", user.getUsername());
			mimeMessageHelper.setFrom(new InternetAddress(env.getProperty("spring.mail.username")));
			mimeMessageHelper.setTo(new InternetAddress(email));
			model.put("code", user.getCode());
			// mimeMessageHelper.setText("Hello ");
			mimeMessageHelper.setSubject(
					VelocityEngineUtils.mergeTemplateIntoString(engine, "email-subject.vm", "UTF-8", model));
			mimeMessageHelper
					.setText(VelocityEngineUtils.mergeTemplateIntoString(engine, "email-body.vm", "UTF-8", model));
			log.info("Preparing message for: " + user.getEmail());

			FileSystemResource file = new FileSystemResource(attachment);
			mimeMessageHelper.addAttachment(file.getFilename(), file);
			javaMailSender.send(mimeMessageHelper.getMimeMessage());

		} catch (MessagingException e) {
			e.printStackTrace();
		}

		return user;
	}

}
