package esprit.tn.batchprocessing;

import java.io.IOException;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ui.velocity.VelocityEngineFactory;

import esprit.tn.entities.User;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	@Value("${codeurjc.batch.attachment}")
	private String attachment;
	private final String JOB_NAME = "emailSenderJob";
	private final String STEP_NAME = "emailSenderStep";

	@Bean
	public FlatFileItemReader<User> reader() {
		return new FlatFileItemReaderBuilder<User>().name("userItemReader").resource(new ClassPathResource("data.csv"))
				.delimited().names(new String[] { "username", "code", "email" })
				.fieldSetMapper(new BeanWrapperFieldSetMapper<User>() {
					{
						setTargetType(User.class);
					}
				}).build();
	}

	/*
	 * @Bean public UserItemProcessor processor() { return new UserItemProcessor();
	 * }
	 */

	@Bean
	public UserItemProcessor processor() {
		return new UserItemProcessor(attachment);
	}

	@Bean
	public UserBatchItemWriter writer() {
		return new UserBatchItemWriter();
	}

	@Bean
	public VelocityEngine getVelocityEngine() throws VelocityException, IOException {
		VelocityEngineFactory factory = new VelocityEngineFactory();
		Properties props = new Properties();
		props.put("resource.loader", "class");
		props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		factory.setVelocityProperties(props);
		return factory.createVelocityEngine();
	}

	@Bean(name = "emailSenderJob")
	public Job emailSenderJob() {
		return this.jobBuilderFactory.get(JOB_NAME).start(emailSenderStep()).build();
	}

	@Bean
	public Step emailSenderStep() {
		return this.stepBuilderFactory.get(STEP_NAME).<User, User>chunk(100).reader(reader()).processor(processor())
				.writer(writer()).allowStartIfComplete(true).build();
	}

}