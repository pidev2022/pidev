package esprit.tn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiDevApplication {
	
	
		
	public static void main(String[] args) throws IOException {
		
		
		SpringApplication.run(PiDevApplication.class, args);
	}

}
