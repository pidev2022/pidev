package esprit.tn.dao;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Repository;
@Repository
public interface EmailServiceRepository {

	public void sendEmail(SimpleMailMessage email);

}
