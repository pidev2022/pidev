package esprit.tn.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.tn.entities.Commentaire;

@Repository
public interface CommentaireRepository extends JpaRepository<Commentaire, Long>  {

}
