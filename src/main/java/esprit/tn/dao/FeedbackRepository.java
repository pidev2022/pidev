package esprit.tn.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import esprit.tn.entities.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback , Integer> {

}
