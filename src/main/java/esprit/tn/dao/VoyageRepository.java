package esprit.tn.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import esprit.tn.entities.Voyage;


@Repository
public interface VoyageRepository  extends JpaRepository<Voyage, String>{
	@Query("select v from Voyage v where v.id_voyage = :x")
	public Voyage findVoyageById( @Param("x")Long id_voyage );
	@Query("select v from Voyage v where v.id_voyage = :x")
	public Long findByIdVoyage( @Param("x")Long id_voyage );
	@Query("select v from Voyage v where v.label = :x")
	public Voyage findByVoyageLabel( @Param("x")String label );

}
