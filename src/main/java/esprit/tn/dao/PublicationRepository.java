package esprit.tn.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import esprit.tn.entities.Publication;


@Repository
public interface PublicationRepository extends JpaRepository<Publication, Long> {

}
