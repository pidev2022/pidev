package esprit.tn.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.tn.entities.PasswordResetToken;
import esprit.tn.entities.Role;

@Repository
public interface PasswordResetRepository extends JpaRepository<PasswordResetToken, String> {

}