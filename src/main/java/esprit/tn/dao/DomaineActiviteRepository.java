package esprit.tn.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import esprit.tn.entities.DomaineActivite;


@Repository
public interface DomaineActiviteRepository  extends JpaRepository<DomaineActivite ,Long>  {

	@Query("select da from DomaineActivite da where da.id = :x")

	public DomaineActivite findByIdDa( @Param("x")Long id ); 

	
}
