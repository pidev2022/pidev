package esprit.tn.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import esprit.tn.entities.Entreprise;


@Repository
public interface EntrepriseRepository  extends JpaRepository<Entreprise,Long>  {
	@Query("select e from Entreprise e where e.id = :x")
	public Entreprise findByIdEn( @Param("x")Long id ); 
}
