package esprit.tn.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.tn.entities.Reactions;

@Repository
public interface ReactionRepository extends JpaRepository<Reactions, String>  {

}
