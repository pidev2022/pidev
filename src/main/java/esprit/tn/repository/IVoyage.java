package esprit.tn.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import esprit.tn.entities.User;
import esprit.tn.entities.Voyage;

 
@Component
public interface IVoyage 
{ 
	public List<Voyage> getVoyage(); 
	public Voyage getVoyage( String label );   
	public Voyage saveVoyage(Voyage u);
	public boolean deleteVoyage(String label);
}
