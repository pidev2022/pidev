package esprit.tn.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import esprit.tn.entities.User;

 
 
public interface IUserMetier 
{ 
	public List<User> getUsers(); 
	public Page<User> getUsers(int page, int size); 
	public Page<User> getUsersByMotCle(String mc,int page, int size); 
	public User getUser( String username );   
	public User saveUser(User u);
	public boolean deleteUser(String username);
    public Optional<User> findUserByResetToken(String resetToken);

}
