package esprit.tn.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import esprit.tn.dao.FeedbackRepository;
import esprit.tn.dao.UserRepository;
import esprit.tn.entities.Feedback;

@Service
public class FeedbackMetierImpl implements FeedbackMetier {
	
	@Autowired
	private FeedbackRepository feedbackRepository;

	@Autowired
	UserRepository userRep;
	@Override
	public List<Feedback> listFeedbacks() {
		// TODO Auto-generated method stub
		return feedbackRepository.findAll();
	}

	@Override
	public void addFeedback(Feedback feedback, String user) {
		// TODO Auto-generated method stub
		feedback.setUser(userRep.getById(user));
		feedbackRepository.save(feedback);
	}
	
}
