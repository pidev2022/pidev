package esprit.tn.repository;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import esprit.tn.dao.DomaineActiviteRepository;
import esprit.tn.dao.EntrepriseRepository;
import esprit.tn.entities.DomaineActivite;
import esprit.tn.entities.Entreprise;

@Service
public class EntrepriseMetierImpl implements IEntrepriseMetier {


	@Autowired
    EntrepriseRepository entrepriseRepoistory;
	
	@Autowired
    DomaineActiviteRepository domaineActiviteRepoistory;
	
	@Override
	public Entreprise addEntreprise(Entreprise entreprise) {
		return entrepriseRepoistory.save(entreprise);
	}

	@Override
	public void deleteEntrepriseById(Long id) {
		entrepriseRepoistory.delete(entrepriseRepoistory.findByIdEn(id));		
	}

	@Override
	public Entreprise updateEntreprise(Entreprise entreprise) {
		return entrepriseRepoistory.save(entreprise);

	}

	@Override
	public List<Entreprise> retrieveAllEntreprise() {


		return entrepriseRepoistory.findAll();

	}

	@Override
	public Entreprise retrieveEntrepriseById(Long id) {

		return entrepriseRepoistory.findByIdEn(id);
	}

	@Override
	public void affecterDomaineActivite(Long id, Long entrepriseId) {

		Entreprise entreprise = entrepriseRepoistory.findByIdEn(entrepriseId);
		DomaineActivite da = domaineActiviteRepoistory.findByIdDa(id);
		
		da.setEntreprise(entreprise);
		domaineActiviteRepoistory.save(da);
	}

	@Override
	public List<String> getAllDomaineActiviteNamesByEntreprise(Long entrepriseId) {
		Entreprise entreprise = entrepriseRepoistory.findByIdEn(entrepriseId);
		List<String> domaineActiviteNames = new ArrayList<>();
		for(DomaineActivite da : entreprise.getDomainesActivites()){
			domaineActiviteNames.add(da.getName());
		}
		
		return domaineActiviteNames;
	}

	
	public boolean isValidEmailAddress(String email) {
		   boolean result = true;
		   try {
		      InternetAddress emailAddr = new InternetAddress(email);
		      emailAddr.validate();
		   } catch (AddressException ex) {
		      result = false;
		   }
		   return result;
		}


	
}
