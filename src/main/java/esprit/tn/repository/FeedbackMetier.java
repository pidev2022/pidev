package esprit.tn.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import esprit.tn.entities.Feedback;
import esprit.tn.entities.Reclamation;

@Component
public interface FeedbackMetier {
	public List<Feedback> listFeedbacks();
	public void addFeedback(Feedback feedback , String user);
}
