package esprit.tn.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import esprit.tn.entities.Entreprise;

@Component
public interface IEntrepriseMetier {

	
	Entreprise addEntreprise(Entreprise entreprise);
	void deleteEntrepriseById(Long id);
	Entreprise updateEntreprise(Entreprise entreprise);
	List<Entreprise> retrieveAllEntreprise(); 
	Entreprise retrieveEntrepriseById(Long id);
	public void affecterDomaineActivite(Long daId, Long entrepriseId);
	List<String> getAllDomaineActiviteNamesByEntreprise(Long entrepriseId);	
	
	public boolean isValidEmailAddress(String email);
}
