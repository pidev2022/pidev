package esprit.tn.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import esprit.tn.entities.DomaineActivite;
import esprit.tn.entities.Entreprise;


@Component
public interface IDomaineActiviteMetier {

	public void saveDomaineActivite(DomaineActivite domaineActivite);

	public List<DomaineActivite> listDomaineActivites();

	public List<DomaineActivite> getDomaineActiviteByEntreprise(Entreprise entreprise);

	public DomaineActivite getDomaineActiviteById(Long id);

	public void updateDomaineActivite(DomaineActivite domaineActivite);

	public void deleteDomaineActivite(Long id);

}
