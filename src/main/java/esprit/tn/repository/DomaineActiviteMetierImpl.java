package esprit.tn.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import esprit.tn.dao.DomaineActiviteRepository;
import esprit.tn.entities.DomaineActivite;
import esprit.tn.entities.Entreprise;


@Service
public class DomaineActiviteMetierImpl implements IDomaineActiviteMetier {

	@Autowired

	DomaineActiviteRepository da;


	@Override

	public void saveDomaineActivite(DomaineActivite domaineActivite) {

		// TODO Auto-generated method stub

		da.save(domaineActivite);

	}


	@Override

	public List<DomaineActivite> listDomaineActivites() {

		// TODO Auto-generated method stub

		return da.findAll();

	}


	@Override

	public List<DomaineActivite> getDomaineActiviteByEntreprise(Entreprise entreprise) {

		// TODO Auto-generated method stub

		return null;

	}


	@Override

	public DomaineActivite getDomaineActiviteById(Long id) {

		// TODO Auto-generated method stub

		return da.findByIdDa(id);

	}


	@Override

	public void updateDomaineActivite(DomaineActivite domaineAvtivite) {

		// TODO Auto-generated method stub

		 da.save(domaineAvtivite);

	}


	@Override

	public void deleteDomaineActivite(Long id) {

		// TODO Auto-generated method stub

		da.delete(da.findByIdDa(id));

	}
	}

