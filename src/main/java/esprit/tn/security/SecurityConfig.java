package esprit.tn.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired private DataSource dataSource; 

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.jdbcAuthentication().dataSource(dataSource)
		.usersByUsernameQuery("select username , password , active from user where username=?")
		.authoritiesByUsernameQuery("select username as principal, role_id from users_roles ur,role r where ur.username=? and ur.role_id=r.id")
		.passwordEncoder(new BCryptPasswordEncoder());
	}

	
	
	
	@Override 
	protected void configure(HttpSecurity http) throws Exception
	{
	
		http.csrf().disable();
		http.authorizeRequests().antMatchers("/root").hasRole("root")
		.antMatchers("/employee").hasRole("employee")
		.antMatchers("/users/**").permitAll()
		.antMatchers("/public/**").permitAll().and().formLogin();



	}

}
