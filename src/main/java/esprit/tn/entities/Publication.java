package esprit.tn.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;


@Entity
public class Publication {
 @Id
 @NotNull
 @GeneratedValue(strategy = GenerationType.AUTO)
 private Long id;
 private String titre;
 private Date date;
 private String author;
 private String details;
 private Long likes;
 
 @OneToMany(mappedBy="publication",
		 cascade = CascadeType.ALL,
          fetch=FetchType.LAZY) 
 List<Commentaire> commentaire;

public String getTitre() {
	return titre;
}
public void setTitre(String titre) {
	this.titre = titre;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public String getDetails() {
	return details;
}
public void setDetails(String details) {
	this.details = details;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public List<Commentaire> getCommentaire() {
	return commentaire;
}
public void setCommentaire(List<Commentaire> commentaire) {
	this.commentaire = commentaire;
}
public Long getLikes() {
	return likes;
}
public void setLikes(Long likes) {
	this.likes = likes;
}
 
}
