package esprit.tn.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Commentaire {
	
	
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;
private String details;

@JoinColumn(name="id_publication")
@JsonIgnore
@ManyToOne
private Publication publication;

public Long getId() {
	return id;
}
public Commentaire() {
	super();
}
public Commentaire(Long id, String details, Publication publication) {
	super();
	this.id = id;
	this.details = details;
	this.publication = publication;
}
public void setId(Long id) {
	this.id = id;
}
public String getDetails() {
	return details;
}
public void setDetails(String details) {
	this.details = details;
}
public Publication getPublication() {
	return publication;
}
public void setPublication(Publication publication) {
	this.publication = publication;
}

}
