package esprit.tn.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Reactions {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private String idrea;
private String type;
public String getId() {
	return idrea;
}
public void setId(String id) {
	this.idrea = id;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}

}
