package esprit.tn.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name ="feedback")
	public class Feedback implements Serializable {
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String comments;
	private String sentiment;
	@ManyToOne
	private User user; 
	public Feedback(@NotNull int id, String comments, String sentiment) {
		super();
		this.id = id;
		this.comments = comments;
		this.sentiment = sentiment;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getSentiment() {
		return sentiment;
	}
	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}
	@Override
	public String toString() {
		return "Feedback [id=" + id + ", comments=" + comments + ", sentiment=" + sentiment + "]";
	}
	public Feedback() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
	
	
	
}
