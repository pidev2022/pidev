package esprit.tn.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Reclamation {
	@Id 
	@NotNull 
	@GeneratedValue Long id;
	
	private String titre;
	private String description;	
	private String etat ="en attente";	 
	private Gravite gravite ;	
	
	
	
	@ManyToOne
	private User user  ; 


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitre() {
		return titre;
	}


	public void setTitre(String titre) {
		this.titre = titre;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getEtat() {
		return etat;
	}


	public void setEtat(String etat) {
		this.etat = etat;
	}


	

	


	public Gravite getGravite() {
		return gravite;
	}


	public void setGravite(Gravite gravite) {
		this.gravite = gravite;
	}


	public Reclamation(@NotNull Long id, String titre, String description, String etat, Gravite gravite) {
		super();
		this.id = id;
		this.titre = titre;
		this.description = description;
		this.etat = etat;
		this.gravite = gravite;		
	}

	public Reclamation() {
		super();
	}


	@Override
	public String toString() {
		return "Reclamation [id=" + id + ", titre=" + titre + ", description=" + description + ", etat=" + etat
				+ ", gravite=" + gravite +"]";
	}
	

}