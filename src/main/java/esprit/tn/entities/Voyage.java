package esprit.tn.entities;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Voyage {
	public Voyage() {}

	public Voyage(@NotNull Long id_voyage, String label, Date date_deb, Date date_fin, String destination_deb,
			String destination_arriv, String type, String description) {
		super();
		this.id_voyage = id_voyage;
		this.label = label;
		this.date_deb = date_deb;
		this.date_fin = date_fin;
		this.destination_deb = destination_deb;
		this.destination_arriv = destination_arriv;
		this.type = type;
		this.description = description;
	}
	@Id @NotNull  
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_voyage;
	private String label;
	private Date date_deb;
	private Date date_fin;
	private String destination_deb;
	private String destination_arriv;
	private String type;
	private String description;
	public Long getIdVoyage() {
		return id_voyage;
	}
	public void setIdVoyage(Long id_voyage) {
		this.id_voyage = id_voyage;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Date getDateDeb() {
		return date_deb;
	}
	public void setDateDeb(Date date_deb) {
		this.date_deb = date_deb;
	}
	public Date getDateFin() {
		return date_fin;
	}
	public void setDateFin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getDestinationDeb() {
		return destination_deb;
	}
	public void setDestinationDeb(String destination_deb) {
		this.destination_deb = destination_deb;
	}
	public String getDestinationArriv() {
		return destination_arriv;
	}
	public void setDestinationArriv(String destination_arriv) {
		this.destination_arriv = destination_arriv;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
/*	@OneToMany(cascade = CascadeType.ALL ,mappedBy ="Voyage" ,fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<Employee> employee;
*/

	
	    
	

}
