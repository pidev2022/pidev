package esprit.tn.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class DomaineActivite  {

	
	@Id 

	@NotNull 

	@GeneratedValue Long id;

	
	private String name;
	
	public Long getId() {

		return id;

	}
	public void setId(Long id) {

		this.id = id;

	}

	
	
	@JsonIgnore

	@JoinColumn(name="id_entreprise")
	@ManyToOne
	private Entreprise entreprise;

	
	
	public DomaineActivite(String name) {
		this.name = name;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}
	
	public DomaineActivite(@NotNull Long id, String name) {

		super();

		this.id = id;

		this.name = name;

	

	}

	
	public DomaineActivite() {

		
	}

	@Override

	public String toString() {

		return "DomaineActivite [id=" + id + ", name=" + name  +"]";

	}

	


}
